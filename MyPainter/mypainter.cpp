#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(800, 600);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::KresliClicked()
{
	//vymaze kresliacu plochu
	paintWidget.clearImage();

	//nastavi zadanu farbu hranici
	nastavi_farbu_hranici();

	//nastavi zadanu farbu vyplne
	nastavi_farbu_vyplne();

	//vykresli hranicu
	paintWidget.dda();

	//vyplni oblast
	paintWidget.vypln_oblast();
}

void MyPainter::nastavi_farbu_hranici() {
	if (ui.lineEdit_3->text() == "" && ui.lineEdit_4->text() == "" && ui.lineEdit_5->text() == "") {
		paintWidget.nastav_farbu_hranici();
	}
	else {
		paintWidget.nastav_farbu_hranici(ui.lineEdit_3->text().toInt(), ui.lineEdit_4->text().toInt(), ui.lineEdit_5->text().toInt());
	}
}

void MyPainter::nastavi_farbu_vyplne() {
	if (ui.lineEdit->text() == "" && ui.lineEdit_2->text() == "" && ui.lineEdit_6->text() == "") {
		paintWidget.nastav_farbu_vyplne();
	}
	else {
		paintWidget.nastav_farbu_vyplne(ui.lineEdit->text().toInt(), ui.lineEdit_2->text().toInt(), ui.lineEdit_6->text().toInt());
	}
}