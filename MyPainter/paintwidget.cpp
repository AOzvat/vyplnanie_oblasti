#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 5;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	modified = true;
	painter.drawPoint(lastPoint);

	body.push_back(lastPoint);

	update();
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::nastav_farbu_hranici(int red, int green, int blue, int a) {
	farba_hranici.setRgb(red, green, blue, a);
}

void PaintWidget::nastav_farbu_vyplne(int red, int green, int blue, int a) {
	farba_vyplne.setRgb(red, green, blue, a);
}

void PaintWidget::dda() {
	int x1, y1, x2, y2, dx, dy, krok;
	float zvacsene_x, zvacsene_y, x, y;

	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	
	//prejde vsetky body
	for (int i = 0; i < body.size(); i++) {
		//zaradi do premennych body
		if (i != body.size() - 1) {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}
		else {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

		//vypocita rozdiel medzi bodmi
		dx = x2 - x1;
		dy = y2 - y1;

		//pre zaciatok pripadi zaciatocny bod
		x = x1;
		y = y1;

		//rozhodne sa v ktorom smere sa bude krokuvat
		if (abs(dx) > abs(dy)) {
			krok = abs(dx);
		}
		else {
			krok = abs(dy);
		}

		//o kolko sa budu suradnice zvacsovat
		// if dx > dy -> zvacsenie_x = 1
		// if dx < dy -> zvacsenie_y = 1
		zvacsene_x = dx / (float)krok;
		zvacsene_y = dy / (float)krok;

		//zvacsovanie a vykreslovanie "ciare"
		for (int j = 0; j < krok; j++) {
			x += zvacsene_x;
			y += zvacsene_y;
			painter.drawPoint(x, y);
		}
	}
	
	update();
}

void PaintWidget::vytvorenie_th() {
	int x1, y1, x2, y2, dx, dy;
	TH tmp;

	//nastavy y_min a y_max na pociatocne hodnoty
	y_min = 10000;
	y_max = 0;

	//najprv vycisti vector
	hrany.clear();

	//prejde vsetky body
	for (int i = 0; i < body.size(); i++) {
		//zaradi do premennych body
		if (i != body.size() - 1) {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}
		else {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

		//vodorovne hrany sa vyhodia
		if ((y2 - y1) != 0) {
			//aby vzdy bola s hora nadol hrana
			if (y1 > y2) {
				std::swap(x1, x2);
				std::swap(y1, y2);
			}

			//skratime hranicu o jeden pixel v y-osi
			y1 -= 1;

			//vypocita rozdiel medzi bodmi
			dx = x2 - x1;
			dy = y2 - y1;

			//vlozime hranu do tabulky hran
			tmp.x1 = x1;
			tmp.y1 = y1;
			tmp.y2 = y2;
			tmp.w = dx / (float)dy;

			//nastav y_min a y_max
			if (y_min > y1) {
				y_min = y1;
			}

			if (y_max < y2) {
				y_max = y2;
			}

			hrany.push_back(tmp);
		}
	}

	//vysortuje hrany podla y1 suradnici
	sort_y();
}

void PaintWidget::sort_y() {
	//sortovaci algoritmus pre tabulku hran podla y1

	for (int i = 0; i < (hrany.size() - 1); i++) {
		for (int j = 0; j < (hrany.size() - i - 1); j++) {
			if (hrany[j].y1 > hrany[j + 1].y1) {
				std::swap(hrany[j], hrany[j + 1]);
			}
		}
	}
}

void PaintWidget::sort_x() {
	//sortovaci algoritmus pre tabulku aktivnych hran podla x

	for (int i = 0; i < (aktivne_hrany.size() - 1); i++) {
		for (int j = 0; j < (aktivne_hrany.size() - i - 1); j++) {
			if (aktivne_hrany[j].x1 > aktivne_hrany[j + 1].x1) {
				std::swap(aktivne_hrany[j], aktivne_hrany[j + 1]);
			}
		}
	}
}

void PaintWidget::vypln_oblast() {
	std::vector<int> priesecniky;
	int x1, x2;

	QPainter painter(&image);
	painter.setPen(QPen(farba_vyplne, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//vytvorenie tabulky aktivnych hran
	vytvorenie_th();
	
	//najprv vycisti vector a nastavy priesecniky
	aktivne_hrany.clear();
	priesecniky.resize(hrany.size());

	//zvacsujeme y vzdy o 1
	for (int y = y_min; y < y_max; y++) {
		//presun hran s tabulky hran do tabulky aktivnych hran, kerie maju y1 == y
		for (int i = 0; i < hrany.size(); i++) {
			if (hrany[i].y1 == y) {
				aktivne_hrany.push_back(hrany[i]);
			}
		}

		//usporiadaj tabulku aktivnych hran podla x
		sort_x();

		//vypocita priesecnik pre kazdy aktivnu hranu
		//vyradi hrany s tabulky aktivnych hran ak y2 == y
		for (int i = 0; i < aktivne_hrany.size(); i++) {
			if (aktivne_hrany[i].y2 == y) {
				aktivne_hrany.erase(aktivne_hrany.begin() + i);
			}
			else {
				priesecniky[i] = aktivne_hrany[i].w * y - aktivne_hrany[i].w * aktivne_hrany[i].y1 + aktivne_hrany[i].x1;
			}
		}

		//zoradi priesecniky
		for (int i = 0; i < (priesecniky.size() - 1); i++) {
			for (int j = 0; j < (priesecniky.size() - i - 1); j++) {
				if (priesecniky[j] > priesecniky[j + 1]) {
					std::swap(priesecniky[j], priesecniky[j + 1]);
				}
			}
		}

		//zacne vyfarbovat
		//ak sa priesecnike do paru
		if (priesecniky.size() % 2 == 0) {
			for (int i = 0; i < priesecniky.size(); i += 2) {
				for (int x = priesecniky[i]; x < priesecniky[i + 1]; x++) {
					painter.drawPoint(x, y);
				}
			}
		}

		//ak sa priesecnike ni do paru
		if (priesecniky.size() % 2 == 1) {
			for (int i = 0; i < priesecniky.size() - 1; i += 2) {
				for (int x = priesecniky[i]; x < priesecniky[i + 1]; x++) {
					painter.drawPoint(x, y);
				}
			}
		}

		//ak je tabulka hran a tabulka aktivnych hran prazdna skonci
		if (hrany.size() == 0 && aktivne_hrany.size() == 0) {
			break;
		}
	}

	//vycisti vector hrany a aktivne_hrany a tiez body
	hrany.clear();
	aktivne_hrany.clear();
	body.clear();
	priesecniky.clear();
}