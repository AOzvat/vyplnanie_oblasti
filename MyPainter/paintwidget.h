#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <cmath>
#include <iostream>
#include <QLineEdit>

//veci ktore obsahuje tabulka hran a tabulka aktivnych hran
typedef struct TH {
	int y1;
	int x1;
	int y2;
	float w;
}TH;

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

	public slots:
	void clearImage();
	void nastav_farbu_hranici(int red = 0, int green = 0, int blue = 0, int a = 255);
	void nastav_farbu_vyplne(int red = 255, int green = 0, int blue = 0, int a = 255);
	void dda();
	void vytvorenie_th();
	void sort_y(); 
	void sort_x();
	void vypln_oblast();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	QColor farba_hranici;
	QColor farba_vyplne;
	std::vector<QPoint> body;
	std::vector<TH> hrany;
	std::vector<TH> aktivne_hrany;
	int y_min;
	int y_max;
};

#endif // PAINTWIDGET_H